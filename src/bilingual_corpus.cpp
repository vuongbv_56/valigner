// Copyright 2015 by Bui Van Vuong
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include <iostream>
#include <sstream>
#include <fstream>
#include <src/types.h>
#include <vector>
#include <algorithm>

#include "src/dict.h"
#include "src/bilingual_corpus.h"

sentence& TrainingInstance::get_source() {
  return src;
}
sentence& TrainingInstance::get_target() {
  return trg;
}

std::vector<context_t>& TrainingInstance::get_source_context() {
  return src_context;
}

void TrainingInstance::set_alignment(alignment_t &alignment) {
  this->alignment = alignment;
}

alignment_t& TrainingInstance::get_alignment() {
  return alignment;
}

static const std::string src_null = "<src-null>";
Corpus::Corpus(const char* src_filename, const char* trg_filename, std::vector<std::string> src_context_filenames,
	       bool use_null, Dict* src_dp, Dict* trg_dp)
  : src_context_d(src_context_filenames.size()),
    number_of_context(src_context_filenames.size()),
    has_alignment(false) {

  if (src_dp)
    this->src_d = *src_dp;
  if (trg_dp)
    this->trg_d = *trg_dp;
  
  std::ifstream src_fs(src_filename);
  std::ifstream trg_fs(trg_filename);
  std::vector<std::ifstream> src_context_fs(src_context_filenames.size());
  for (unsigned i = 0; i < src_context_fs.size(); i++)
    src_context_fs[i].open(src_context_filenames[i]);
  
  while (true) {
    std::string src, trg;
    std::vector<std::string> context;
    getline(src_fs, src);
    getline(trg_fs, trg);

    bool okay = src_fs.good() && trg_fs.good();
    for (std::ifstream& c_fs: src_context_fs) {
      std::string s;
      getline(c_fs, s);
      context.push_back(s);
      okay = okay && c_fs.good();
    }

    if (!okay)
      break;

    TrainingInstance instance = TrainingInstance(*this, src, trg);
    instance.set_source_context(context);
  
    instances.push_back(instance);
  }  
}

void Corpus::set_alignment_file(const char* alignment_file) {
  this->has_alignment = true;
  std::ifstream a_file(alignment_file);
  std::vector<TrainingInstance>& instances = get_training_instances();
  for (std::vector<TrainingInstance>::iterator it = instances.begin();
       it != instances.end();
       it++) {
    std::string line;
    getline(a_file, line);
    if (! a_file.good())
      break;
    sentence& src = it->get_source();
    sentence& trg = it->get_target();
    alignment_t alignment(trg.size());
    for (wordind_t i = 0; i < trg.size(); i++)
      alignment[i] = src.size()-1;
    std::istringstream iss(line);
    do {
      int i, j;
      char c;
      iss >> j;
      iss >> c;
      iss >> i;
      if (!iss) break;
      if (c == '-')
	alignment[i] = j;
    } while (true);

    it->set_alignment(alignment);
  }
}

void Corpus::get_canonical_form(sentence& src, sentence& trg,
				std::string& canon_src, std::string& canon_trg) {
  size_t m = trg.size();
  size_t n = src.size();
  canon_src = "";
  canon_trg = "";
  for (wordind_t i = 0; i < m; i++)
    canon_trg += trg_d.Convert(trg[i]) + " ";
  for (wordind_t j = 0; j < n; j++)
    canon_src += src_d.Convert(src[j]) + " ";
}

std::vector<TrainingInstance>& Corpus::get_training_instances() {
  return instances;
}


void TrainingInstance::set_source_context(std::vector<std::string>& src_context) {
  std::vector<sentence> context(src_context.size());
  for (unsigned k = 0; k < src_context.size(); k++)
    corpus.src_context_d[k].ConvertWhitespaceDelimitedLine(src_context[k], &context[k]);
  for (wordind_t i = 0; i < this->src.size(); i++) {
    context_t c;
    for (sentence& con: context)
      c.push_back(con[i]);
    this->src_context.push_back(c);
  }
  if (corpus.use_null) {
    context_t null_c;
    for (Dict& d: corpus.src_context_d)
      null_c.push_back(d.Convert(src_null));
    this->src_context.push_back(null_c);
  }
}

TrainingInstance::TrainingInstance(Corpus& corpus, std::string& src, std::string& trg)
  : corpus(corpus) {
  corpus.src_d.ConvertWhitespaceDelimitedLine(src, &(this->src));
  corpus.trg_d.ConvertWhitespaceDelimitedLine(trg, &(this->trg));
    
  if (corpus.use_null)
    this->src.push_back(corpus.src_d.Convert(src_null));
}

int Corpus::get_number_of_context() {
  return number_of_context;
}
