#include "ttables.h"
#include "src/types.h"
#include "src/ntables.h"

NTable::NTable(Corpus& corpus)
  : TTable(corpus) {
}
void NTable::ExportToFile(const char* filename, Dict& sd) {
  std::ofstream file(filename);
  for (unsigned i = 0; i < ttable.size(); ++i) {
    const std::string& a = sd.Convert(i);
    std::unordered_map<unsigned, prob_t>& cpd = ttable[i].prob_map;
    for (std::unordered_map<unsigned, prob_t>::iterator it = cpd.begin();
	 it != cpd.end(); ++it) {
      unsigned b = it->first;
      prob_t c = it->second;
      file << a << '\t' << b << '\t' << c << std::endl;
    }
  }
  file.close();
}  

