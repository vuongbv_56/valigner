// Copyright 2015 by Bui Van Vuong
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <utility>
#include <fstream>
#include <algorithm>
#include <numeric>
#include <getopt.h>
#include <set>
#include <cstdlib>

#include "src/dict.h"
#include "src/ttables.h"
#include "src/atables.h"
#include "src/ntables.h"
#include "src/utilities.h"
#include "src/types.h"
#include "src/bilingual_corpus.h"
#include "src/dtables.h"
#include "src/model.h"
#include "src/model12.h"
using namespace std;

class Model3: public Model12 {
public:
  bool use_peg;
  VectorTable genNull;
  NTable ntable;
  DTable dtable;
  Model3(Model12 &m, bool use_peg=true);

  void em_iteration();
  void do_statistics(TrainingInstance& instance);

  vector<unsigned> fertility_count(alignment_t& al, TrainingInstance& instance);
  logprob_t probability(alignment_t& al, TrainingInstance& instance);

  alignment_t align_words(TrainingInstance& instance);

  bool good_fertility(vector<unsigned> fertility, TrainingInstance& instance);
  
  vector<pair<alignment_t, logprob_t>> sample(TrainingInstance& instance, bool use_peg=true);
  vector<pair<alignment_t, logprob_t>> random_sample(TrainingInstance& instance, int nSamples=1000);

  pair<alignment_t, logprob_t> hillclimb(pair<alignment_t, logprob_t>& start, size_t t_pegged,
					 TrainingInstance& instance, bool use_peg = true);
  prob_t score_of_swap(TrainingInstance& instance, alignment_t& alignment, wordind_t first_i, wordind_t second_i);
  prob_t score_of_move(TrainingInstance& instance, alignment_t& alignment, vector<unsigned>& fertility, wordind_t i, wordind_t new_j);
  
  vector<pair<alignment_t, logprob_t>> neighboring(pair<alignment_t, logprob_t>& center, size_t t_pegged,
						       TrainingInstance& instance, bool use_peg = true);
  void export_to_file(const char *lex_filename, const char *pos_filename, const char* a_filename, const char* n_filename);
};
