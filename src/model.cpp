// Copyright 2015 by Bui Van Vuong
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
#include <unordered_map>
#include <iostream>
#include <cstdlib>
#include <cmath>
#include <utility>
#include <fstream>
#include <algorithm>
#include <numeric>
#include <getopt.h>

#include "src/dict.h"
#include "src/ttables.h"
#include "src/atables.h"
#include "src/types.h"
#include "src/bilingual_corpus.h"
#include "src/model.h"

using namespace std;

Model::Model(Corpus &corpus)
  : corpus(corpus), name("noname") {
  this->use_null = corpus.use_null;
}

void Model::set_name(string name) {
  // this->name = name;
}

void Model::train(int n_iterations) {
  for (int iteration = 0; iteration < n_iterations; ++iteration) {
    cerr << "iteration #" << iteration << endl; 
    em_iteration();
    cerr << "perplexity = " << perplexity() << endl;
    // const char* fname = (this->name + "." + to_string(iteration)).c_str();
    // cerr << "writing aligment to " << fname << endl;
    // align_words_in_corpus(this->corpus, fname);
  }
}

void Model::align_words_in_corpus(const char* output_filename) {
  // care about difference between two Corpus
  ofstream os(output_filename);
  vector<TrainingInstance> instances = get_instances_for_training();
  // vector<TrainingInstance> instances = get_instances_for_validating();

  for (vector<TrainingInstance>::iterator it = instances.begin();
       it != instances.end(); it++) {
    sentence& src = it->get_source();
    sentence& trg = it->get_target();

    string canon_src, canon_trg;
    corpus.get_canonical_form(src, trg, canon_src, canon_trg);
    vector<wordind_t> al = align_words(*it);
    // os << canon_src << endl;
    // os << canon_trg << endl;
    for (size_t i = 0; i < trg.size(); i++) {
      if (al[i] != src.size() - 1 || ! use_null)
	os << al[i] << "-" << i;
      // else
      // 	os << "*" << "-" << i;
      os << ((i<trg.size()-1) ? " " : "\n");
    }
    // for (size_t i = 0; i < trg.size(); i++)
    //   os << corpus.trg_d.Convert(trg[i]) << "-" <<
    // 	corpus.src_d.Convert(src[al[i]]) << ((i<trg.size()-1) ? " " : "\n");
  }
  os.close();
}

void Model::em_iteration() {
  vector<TrainingInstance> instances = get_instances_for_training();
  // int count = 0;
  // int total = instances.size();
  
  for (vector<TrainingInstance>::iterator it = instances.begin();
       it != instances.end(); it++) {
    // count++;
    // sentence& src = it->get_source();
    // sentence& trg = it->get_target();
    // cerr << "Do statistics on pair " << count << "(out of " << total << ")" << endl;
    // if (count % (total / 100) == 0) {
    //   cerr << count / (total / 100) << "%\t";
    //   if ((count / (total / 100)) % 10 == 0)
    // 	cerr << endl;
    // }
    // for (wordind_t i = 0; i < src.size(); i++)
    //   cout << src[i] << " ";
    // cout << endl;
    // for (wordind_t i = 0; i < trg.size(); i++)
    //   cout << trg[i] << " ";
    // cout << endl;

    do_statistics(*it);
  }
}

// vector<TrainingInstance> Model::get_instances_for_training() {
//   vector<TrainingInstance>& total = corpus.get_training_instances();
//   vector<TrainingInstance> training(total.begin(), total.begin() + (total.size() * 9/10));
//   return training;
// }

// vector<TrainingInstance> Model::get_instances_for_validating() {
//   vector<TrainingInstance>& total = corpus.get_training_instances();
//   vector<TrainingInstance> validating(total.begin() + (total.size() * 9/10), total.end());
//   return validating;
// }

vector<TrainingInstance> Model::get_instances_for_training() {
  vector<TrainingInstance>& total = corpus.get_training_instances();
  vector<TrainingInstance> training(total.begin(), total.begin() + (int)(total.size() * 0.9));
  return training;
}

vector<TrainingInstance> Model::get_instances_for_validating() {
  vector<TrainingInstance>& total = corpus.get_training_instances();
  vector<TrainingInstance> validating(total.begin() + (int)(total.size() * 0.9), total.end());
  return validating;
}

// vector<TrainingInstance> Model::get_instances_for_training() {
//   vector<TrainingInstance>& total = corpus.get_training_instances();
//   vector<TrainingInstance> training(total.begin() + (total.size() * 2/10), total.end());
//   return training;
// }

// vector<TrainingInstance> Model::get_instances_for_validating() {
//   vector<TrainingInstance>& total = corpus.get_training_instances();
//   vector<TrainingInstance> validating(total.begin(), total.begin() + (total.size() * 1/10));
//   return validating;
// }

// alignment_t Model::align_words(sentence& src, sentence& trg) {
//   vector<wordind_t> al;
//   for (size_t i = 0; i < trg.size(); i++) {
//     vector<prob_t> probs = alignment_probs(i, src, trg, false);
//     size_t j = distance(probs.begin(), max_element(probs.begin(), probs.end()));
//     al.push_back(j);
//   }
//   return al;
// }
  
// prob_t Model::sentence_prob(sentence& src, sentence& trg) {
//   prob_t prob = 1.0;
//   for (size_t i = 0; i < trg.size(); i++) {
//     vector<prob_t> ps = alignment_probs(i, src, trg, false);
//     prob_t sum = accumulate(ps.begin(), ps.end(), 0.0);      
//     prob *= sum;
//   }
//   return prob;
// }
  
// vector<prob_t> Model::alignment_probs(size_t i, sentence& src, sentence& trg,
// 					     bool normalize) {
//   size_t m = trg.size();
//   size_t n = src.size();
//   vector<prob_t> probs(n);

//   for (size_t j = 0; j < n; j++) {
//     probs[j] = lex.prob(src[j], trg[i]);
//     if (use_atable)
//       probs[j] *= atable.prob(j, i, m, n);
//   }

//   if (normalize) {
//     prob_t total = accumulate(probs.begin(), probs.end(), 0.0);
//     for (size_t j = 0; j < n; j++)
//       probs[j] /= total;
//   }
//   return probs;
// }

// void Model::do_statistics(sentence& src, sentence& trg) {
//   for (size_t i = 0; i < trg.size(); i++) { 
//     vector<prob_t> probs = alignment_probs(i, src, trg);

//     size_t m = trg.size();
//     size_t n = src.size();
//     for (size_t j = 0; j < n; j++) {
//       lex.Increment(src[j], trg[i], probs[j]);
//       if (use_atable)
// 	atable.Increment(j, i, m, n, probs[j]);
//     }
//   }
// }

