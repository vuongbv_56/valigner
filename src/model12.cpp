// Copyright 2015 by Bui Van Vuong
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
#include <unordered_map>
#include <iostream>
#include <cstdlib>
#include <cmath>
#include <utility>
#include <fstream>
#include <algorithm>
#include <numeric>
#include <getopt.h>

#include "src/dict.h"
#include "src/ttables.h"
#include "src/atables.h"
#include "src/types.h"
#include "src/bilingual_corpus.h"
#include "src/model.h"
#include "src/model12.h"

#include <gsl/gsl_errno.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_min.h>

using namespace std;

Model12::Model12(Corpus &corpus, bool use_atable):
  Model(corpus),
  lex(corpus),
  statistics(corpus) {
  this->use_atable = use_atable;
  this->alpha = 0.0;
}

// void Model::train(int n_iterations) {
//   for (int iteration = 0; iteration < n_iterations; ++iteration) {
//     cerr << "iteration #" << iteration << "\n";
//     em_iteration();
//   }
// }

void Model12::set_annotated_corpus(Corpus* annotated_corpus) {
  this->annotated_corpus = annotated_corpus;
}

alignment_t Model12::align_words(TrainingInstance& instance) {
  sentence &trg = instance.get_target();
  vector<wordind_t> al;
  for (size_t i = 0; i < trg.size(); i++) {
    vector<prob_t> probs = alignment_probs(i, instance, false);
    size_t j = distance(probs.begin(), max_element(probs.begin(), probs.end()));
    al.push_back(j);
  }
  return al;
}
  
logprob_t Model12::sentence_prob(TrainingInstance& instance) {
  sentence& src = instance.get_source();
  sentence& trg = instance.get_target();
  logprob_t prob = 1.0;
  for (size_t i = 0; i < trg.size(); i++) {
    vector<prob_t> ps = alignment_probs(i, instance, false);
    prob_t sum = accumulate(ps.begin(), ps.end(), 0.0);
    prob *= sum;
    if (! use_atable)
      prob *= 1.0 / src.size();
   }
  return prob;
}

double Model12::perplexity() {
  vector<TrainingInstance> instances = get_instances_for_validating();
  // vector<TrainingInstance> instances = corpus.get_training_instances();
  // vector<TrainingInstance> instances = get_instances_for_training();

  double sum = 0.0;
  int n_alignments = 0;
  for (vector<TrainingInstance>::iterator it = instances.begin();
       it != instances.end(); it++) {
    n_alignments += it->get_target().size();
    logprob_t prob = sentence_prob(*it);
    // cerr << "logprob of a sentence = " << prob.base2() << endl;
    sum += - prob.base2();
  }

  return sum / n_alignments;
}

double Model12::likelihood_of_alignment() { // actualy the average of the opposite
  vector<TrainingInstance>& instances = annotated_corpus->get_training_instances();
  double sum = 0;
  int number_of_positions = 0;
  for (vector<TrainingInstance>::iterator it = instances.begin();
       it != instances.end();
       it++) {
    sentence& trg = it->get_target();
    alignment_t& alignment = it->get_alignment();

    number_of_positions += trg.size();
    for (wordind_t i = 0; i < trg.size(); i++)
      sum += - log(alignment_probs(i, *it)[alignment[i]]);
  }
  return sum / number_of_positions;
}

unsigned Model12::error_counts() {
  vector<TrainingInstance>& instances = annotated_corpus->get_training_instances();
  unsigned count = 0;
  for (vector<TrainingInstance>::iterator it = instances.begin();
       it != instances.end();
       it++) {
    sentence& trg = it->get_target();
    alignment_t& alignment = it->get_alignment();
    alignment_t viterbi_alignment = align_words(*it);
    
    for (wordind_t i = 0; i < trg.size(); i++)
      if (alignment[i] != viterbi_alignment[i])
	count++;
  }
  return count;
}

double Model12::smoothed_error_counts(double alpha) {
  vector<TrainingInstance>& instances = annotated_corpus->get_training_instances();
  double count = 0;
  for (vector<TrainingInstance>::iterator it = instances.begin();
       it != instances.end();
       it++) {
    sentence& trg = it->get_target();
    alignment_t& alignment = it->get_alignment();
    
    for (wordind_t i = 0; i < trg.size(); i++) {
      vector<prob_t> probs = alignment_probs(i, *it);
      double total = 0.0;
      for (prob_t prob: probs)
	total += pow(prob, alpha);

      count += 1 - pow(probs[alignment[i]], alpha) / total;
    }
  }
  return count;
}

vector<prob_t> Model12::alignment_probs(size_t i, TrainingInstance& instance,
					     bool normalize) {
  sentence& src = instance.get_source();
  sentence& trg = instance.get_target();
  
  size_t m = trg.size();
  size_t n = src.size();
  vector<prob_t> probs(n);

  for (size_t j = 0; j < n; j++) {
    probs[j] = lex.prob(src[j], trg[i]);
    if (use_atable)
      probs[j] *= atable.prob(j, i, m, n);
  }

  if (normalize) {
    prob_t total = accumulate(probs.begin(), probs.end(), 0.0);
    for (size_t j = 0; j < n; j++)
      probs[j] /= total;
  }
  return probs;
}

void Model12::do_statistics(TrainingInstance& instance) {
  sentence& src = instance.get_source();
  sentence& trg = instance.get_target();

  for (size_t i = 0; i < trg.size(); i++) { 
    vector<prob_t> probs = alignment_probs(i, instance);

    size_t m = trg.size();
    size_t n = src.size();
    for (size_t j = 0; j < n; j++) {
      lex.Increment(src[j], trg[i], probs[j]);
      if (use_atable)
	atable.Increment(j, i, m, n, probs[j]);
    }
  }
}

void Model12::em_iteration() {
  // vector<TrainingInstance>& instances = corpus.get_training_instances();
  // for (vector<TrainingInstance>::iterator it = instances.begin();
  //      it != instances.end(); it++) {
  //   sentence& src = it->get_source();
  //   sentence& trg = it->get_target();
  //   cout << "Do statistics on" << endl;
  //   for (wordind_t i = 0; i < src.size(); i++)
  //     cout << src[i] << " ";
  //   cout << endl;
  //   for (wordind_t i = 0; i < trg.size(); i++)
  //     cout << trg[i] << " ";
  //   cout << endl;

  //   do_statistics(src, trg);
  // }

  // static int current_iteration = 0;
  // current_iteration++;
  Model::em_iteration();

  // if (use_ttable_smoothing && current_iteration > 3) {
  //   double n = smooth_ttable();
  //   cout << "adding " << n << " n to the table" << endl;
  //   lex.Increment(n);
  // }

  // double low, high;
  // low = 0.01; high = 0.01;
  // for (int i = 0; i < 10; i++) {
  //   low -= 0.001;
  //   high += 0.001;
  //   cout << low << ":\t" << perplexity_wrapper(low, this) << endl;
  //   cout << high << ":\t" << perplexity_wrapper(low, this) << endl;    
  // }

  // low = 0.1; high = 0.1;
  // for (int i = 0; i < 10; i++) {
  //   low -= 0.01;
  //   high += 0.01;
  //   cout << low << ":\t" << perplexity_wrapper(low, this) << endl;
  //   cout << high << ":\t" << perplexity_wrapper(low, this) << endl;    
  // }
  
  lex.Normalize(alpha);
  if (use_atable)
    atable.Normalize();

  if (annotated_corpus) {
    cerr << "perplexity of alignment: " << likelihood_of_alignment() << endl;
    cerr << "alignment error count: " << error_counts() << endl;
    cerr << "smoothed alignment error count: " << smoothed_error_counts() << endl;
  }
}

void Model12::set_smooth_for_ttable(double alpha) {
  this->alpha = alpha;
}

double perplexity_wrapper(double alpha, void *params) {
  Model12* m = new Model12(*((Model12*)params));
  m->set_smooth_for_ttable(alpha);
  m->train(7);
  double perp = m->perplexity();
  delete m;
  return perp;
}

double likelihood_wrapper(double alpha, void *params) {
  Model12* m = new Model12(*((Model12*)params));
  m->set_smooth_for_ttable(alpha);
  m->train(7);
  double likely = m->likelihood_of_alignment();
  delete m;
  return likely;
}

double error_count_wrapper(double alpha, void *params) {
  Model12* m = new Model12(*((Model12*)params));
  m->set_smooth_for_ttable(alpha);
  m->train(7);
  double count = m->error_counts();
  delete m;
  return count;
}
double smoothed_error_count_wrapper(double alpha, void *params) {
  Model12* m = new Model12(*((Model12*)params));
  m->set_smooth_for_ttable(alpha);
  m->train(7);
  double count = m->smoothed_error_counts();
  delete m;
  return count;
}

double smooth_ttable(Model12* model, double (*function)(double, void*)) {
  int status;
  int iter = 0, max_iter = 30;
  const gsl_min_fminimizer_type *T;
  gsl_min_fminimizer *s;
  double m = 0.0001, m_expected = 0.0001;
  double a = 0.0, b = 1.0;
  gsl_function F;

  F.function = function;
  F.params = model;

  // cout << perplexity_wrapper(0.0, model) << endl;
  // cout << perplexity_wrapper(0.01, model) << endl;
  // cout << perplexity_wrapper(10.0, model) << endl;
  // for (double alpha = 0; alpha <= 0.001; alpha += 0.0001)
  //   cerr << "EVALUATING AT " << alpha << " GET " << function(alpha, (void*) model) << endl;
  
  T = gsl_min_fminimizer_brent;
  s = gsl_min_fminimizer_alloc (T);
  gsl_min_fminimizer_set (s, &F, m, a, b);


  printf ("using %s method\n",
          gsl_min_fminimizer_name (s));

  printf ("%5s [%9s, %9s] %9s %10s %9s\n",
          "iter", "lower", "upper", "min",
          "err", "err(est)");

  printf ("%5d [%.7f, %.7f] %.7f %+.7f %.7f\n",
          iter, a, b,
          m, m - m_expected, b - a);

  do
    {
      iter++;
      status = gsl_min_fminimizer_iterate (s);

      m = gsl_min_fminimizer_x_minimum (s);
      a = gsl_min_fminimizer_x_lower (s);
      b = gsl_min_fminimizer_x_upper (s);

      status 
        // = gsl_min_test_interval (a, b, 0.001, 0.0);
        = gsl_min_test_interval (a, b, 0.000001, 0.0);

      if (status == GSL_SUCCESS)
        printf ("Converged:\n");

      printf ("%5d [%.7f, %.7f] "
              "%.7f %+.7f %.7f\n",
              iter, a, b,
              m, m - m_expected, b - a);
    }
  while (status == GSL_CONTINUE && iter < max_iter);

  gsl_min_fminimizer_free (s);

  return m;
}
