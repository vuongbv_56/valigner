#ifndef CPYPDICT_H_
#define CPYPDICT_H_

#include <string>
#include <iostream>
#include <cassert>
#include <fstream>
#include <vector>
#include <set>
#include <unordered_map>

class Dict {
  typedef std::unordered_map<std::string, unsigned, std::hash<std::string> > Map;
 public:
  Dict();

  unsigned max() const;

  static bool is_ws(char x);

  void set_frozen(bool frozen);
  void ConvertWhitespaceDelimitedLine(const std::string& line, std::vector<unsigned>* out);
  /* unsigned Convert(const std::string& word, bool frozen = false); */
  unsigned Convert(const std::string& word);  
  const std::string& Convert(const unsigned id) const;
  unsigned size();
 private:
  std::string b0_;
  std::vector<std::string> words_;
  bool frozen;
  Map d_;
};

void ReadFromFile(const std::string& filename,
                  Dict* d,
                  std::vector<std::vector<unsigned> >* src,
                  std::set<unsigned>* src_vocab);

#endif
