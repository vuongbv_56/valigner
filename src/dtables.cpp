#include "src/atables.h"
#include "src/dtables.h"

prob_t DTable::prob(wordind_t i, wordind_t j, wordind_t m, wordind_t n) {
  return ATable::prob(i, j, n, m);
}
void DTable::Increment(wordind_t i, wordind_t j, wordind_t m, wordind_t n, prob_t x) {
  ATable::Increment(i, j, n, m);
}
void DTable::ExportToFile(const char* filename) {
  std::ofstream file(filename);

  for (std::unordered_map<std::pair<wordind_t, wordind_t>, _ATable, PairHash>::iterator it = atable.begin(); it != atable.end(); ++it) {
    std::pair<wordind_t, wordind_t> length_pair = it->first;
    wordind_t m = length_pair.first;
    wordind_t n = length_pair.second;

    _ATable& table = it->second;
    std::vector<VectorTable> ttable = table.ttable;
    for (wordind_t i = 0; i < m; i++) {
      VectorTable vt = ttable[i];
      for (wordind_t j = 0; j < n; j++) {
	prob_t c = vt.prob(j);
	// swap the position in output
	file << j << " " << i << " " << n << " " << m << "\t" << c << std::endl;
      }
    }
  }
  file.close();
}

