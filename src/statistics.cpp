// Copyright 2015 by Bui Van Vuong
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include <iostream>
#include <fstream>
#include <src/types.h>
#include <vector>
#include <algorithm>
#include "src/statistics.h"

Statistics::Statistics(Corpus& corpus)
  : corpus(corpus),
    src_counts(corpus.src_d.max()+1),
    trg_counts(corpus.trg_d.max()+1),
    src_trg_counts(corpus.src_d.max()+1) {
	// std::cout << "statistics S dict: " << corpus.src_d.size() << std::endl;
  // src_counts.resize(corpus.src_d.size());
  // std::cout << "statistics T dict: " << corpus.trg_d.size() << std::endl;  
  // trg_counts.resize(corpus.trg_d.size());
  
  std::vector<TrainingInstance>& instances = corpus.get_training_instances();
  for (TrainingInstance& instance: instances) {
    sentence& src = instance.get_source();
    sentence& trg = instance.get_target();
    for (unsigned s: src)
      src_counts[s]++;
    for (unsigned t: trg)
      trg_counts[t]++;
    for (unsigned s: src) {
      std::unordered_map<unsigned, unsigned>& counts = src_trg_counts[s];
      for (unsigned t: trg)
	counts[t]++;
    }
  }
}

unsigned Statistics::get_n_source_words() {
  return corpus.src_d.size();
}

unsigned Statistics::get_n_target_words() {
  return corpus.trg_d.size();
}

unsigned Statistics::get_n_occurences_of_source_word(unsigned s) {
  return src_counts[s];
}

unsigned Statistics::get_n_occurences_of_target_word(unsigned t) {
  return trg_counts[t];
}

unsigned Statistics::get_n_co_occurences_of_pair(unsigned s, unsigned t) {
  std::unordered_map<unsigned, unsigned>::iterator it = src_trg_counts[s].find(t);
  if (it == src_trg_counts[s].end())
    return 0;
  else
    return it->second;
}
