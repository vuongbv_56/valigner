// Copyright 2015 by Bui Van Vuong
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
#include <unordered_map>
#include <iostream>
#include <cstdlib>
#include <cmath>
#include <utility>
#include <fstream>
#include <algorithm>
#include <numeric>
#include <getopt.h>

#include "src/dict.h"
#include "src/ttables.h"
#include "src/atables.h"
#include "src/types.h"
#include "src/bilingual_corpus.h"
#include "src/model.h"
#include "src/modelc.h"

using namespace std;

ModelC::ModelC(Corpus &corpus, bool use_atable)
  : Model(corpus),
    lex(corpus),
    ctables(corpus.get_number_of_context()),
    weights(corpus.get_number_of_context()),
    phase(TUNING_TRANSLATION),
    use_atable(use_atable) {
}

alignment_t ModelC::align_words(TrainingInstance& instance) {
  sentence& trg = instance.get_target();
  vector<wordind_t> al;
  for (size_t i = 0; i < trg.size(); i++) {
    vector<prob_t> probs = alignment_probs(i, instance, false);
    size_t j = distance(probs.begin(), max_element(probs.begin(), probs.end()));
    al.push_back(j);
  }
  return al;
}
  
logprob_t ModelC::sentence_prob(TrainingInstance& instance) {
  sentence& src = instance.get_source();
  sentence& trg = instance.get_target();
  
  logprob_t prob = 1.0;
  for (size_t i = 0; i < trg.size(); i++) {
    vector<prob_t> ps = alignment_probs(i, instance, false);
    prob_t sum = accumulate(ps.begin(), ps.end(), 0.0);
    prob *= sum;
    if (! use_atable)
      prob *= 1.0 / src.size();
   }
  // cerr << prob.base2() << endl;
  return prob;
}

double ModelC::perplexity() {
  vector<TrainingInstance> instances = get_instances_for_validating();
  double sum = 0.0;
  int n_alignments = 0;
  for (vector<TrainingInstance>::iterator it = instances.begin();
       it != instances.end(); it++) {
    n_alignments += it->get_target().size();
    logprob_t prob = sentence_prob(*it);
    // cerr << "logprob of a sentence = " << prob.base2() << endl;
    sum += - prob.base2();
  }

  return sum / n_alignments;
}

prob_t ModelC::prob_of_link(wordind_t i, wordind_t j, TrainingInstance& instance) {
  sentence& src = instance.get_source();
  sentence& trg = instance.get_target();
  vector<context_t>& context = instance.get_source_context();

  size_t m = trg.size();
  size_t n = src.size();

  prob_t score = 0.0;
  context_t& c = context[j];
  for (unsigned k = 0; k < c.size(); k++) {
    score += weights.prob(k) * ctables[k].prob(c[k], trg[i]);
  }
  if (use_atable)
    score *= atable.prob(j, i, m, n);
  
  return score;
}

vector<prob_t> ModelC::alignment_probs(size_t i, TrainingInstance& instance, bool normalize) {
  size_t n = instance.get_source().size();
  vector<prob_t> probs(n);

  for (size_t j = 0; j < n; j++)
    probs[j] = prob_of_link(i, j, instance);

  if (normalize) {
    prob_t total = accumulate(probs.begin(), probs.end(), 0.0);
    for (size_t j = 0; j < n; j++)
      probs[j] /= total;
  }
  return probs;
}

void ModelC::do_statistics(TrainingInstance& instance) {
  static int count = 0;

  count++;
  if ((count % 10 == 0 && phase == TUNING_TRANSLATION) ||
      (count % 10 != 0 && phase == TUNING_WEIGHT))
    return;

  vector<context_t>& context = instance.get_source_context();
  sentence& src = instance.get_source();

  sentence& trg = instance.get_target();
  
  for (size_t i = 0; i < trg.size(); i++) { 
    vector<prob_t> probs = alignment_probs(i, instance);

    size_t m = trg.size();
    size_t n = src.size();
    for (size_t j = 0; j < n; j++) {
      context_t c = context[j];
      // lex.Increment(src[j], trg[i], probs[j]);
      if (phase == TUNING_TRANSLATION) {
	for (unsigned k = 0; k < c.size(); k++)
	  ctables[k].Increment(c[k], trg[i], weights.prob(k) * probs[j]);
	if (use_atable)
	  atable.Increment(j, i, m, n, probs[j]);
      } else {
	prob_t normal = 0.0;
	for (unsigned k = 0; k < c.size(); k++)
	  normal += ctables[k].prob(c[k], trg[j]);
	for (unsigned k = 0; k < c.size(); k++)
	  weights.Increment(k, ctables[k].prob(c[k], trg[j]) / normal * probs[j]);
      }
    }
  }
}

void ModelC::em_iteration() {
  // vector<TrainingInstance>& instances = corpus.get_training_instances();
  // for (vector<TrainingInstance>::iterator it = instances.begin();
  //      it != instances.end(); it++) {
  //   sentence& src = it->get_source();
  //   sentence& trg = it->get_target();
  //   cout << "Do statistics on" << endl;
  //   for (wordind_t i = 0; i < src.size(); i++)
  //     cout << src[i] << " ";
  //   cout << endl;
  //   for (wordind_t i = 0; i < trg.size(); i++)
  //     cout << trg[i] << " ";
  //   cout << endl;

  //   do_statistics(src, trg);
  // }

  
  if (phase == TUNING_TRANSLATION)
    cout << "Running iteration for TRANSLATION" << endl;
  else
    cout << "Running iteration for WEIGHT" << endl;
  
  Model::em_iteration();
  // lex.Normalize();
  if (phase == TUNING_WEIGHT) {
    weights.Normalize();
  } else {
    for (unsigned k = 0; k < ctables.size(); k++)
      ctables[k].Normalize();
    if (use_atable)
      atable.Normalize();
  }
  
  if (phase == TUNING_WEIGHT) {
    phase = TUNING_TRANSLATION;
    cout << "WEIGHT: ";
    for (int k = 0; k < corpus.get_number_of_context(); k++)
      cout << weights.prob(k) << " ";
    cout << endl;
  } else {
    phase = TUNING_WEIGHT;
    em_iteration();
  }
}
