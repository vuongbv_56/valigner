// Copyright 2015 by Bui Van Vuong
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <utility>
#include <fstream>
#include <algorithm>
#include <numeric>
#include <getopt.h>
#include <set>
#include <cstdlib>

#include "src/dict.h"
#include "src/ttables.h"
#include "src/atables.h"
#include "src/ntables.h"
#include "src/utilities.h"
#include "src/types.h"
#include "src/bilingual_corpus.h"
#include "src/dtables.h"
#include "src/model.h"
#include "src/model3.h"
using namespace std;

Model3::Model3(Model12 &m, bool use_peg): Model12(m), use_peg(use_peg), genNull(2), ntable(corpus) {
  if (!use_null)
    cerr << "Model 3 without NULL feature not supported" << endl;

  use_atable = true;
}

void Model3::em_iteration() {
  Model12::em_iteration();
  dtable.Normalize();
  ntable.Normalize();
  genNull.Normalize();
}

void Model3::do_statistics(TrainingInstance& instance) {
  // cerr << "src_len = " << src.size() << " trg_len = " << trg.size() << "\n";
  vector<pair<alignment_t, logprob_t>> sample_space = sample(instance, use_peg);
  // vector<pair<alignment_t, logprob_t>> sample_space = random_sample(instance, 1000);

  // cerr << "n_samples = " << sample_space.size() << "\n";
  vector<logprob_t> probabilites;
  // int cnt=0;

  logprob_t total_weight = 0.0;
  for (vector<pair<alignment_t, logprob_t>>::iterator it = sample_space.begin();
       it != sample_space.end(); ++it) {
    total_weight += it->second;
  }

  sentence& src = instance.get_source();
  sentence& trg = instance.get_target();

  for (vector<pair<alignment_t, logprob_t>>::iterator it = sample_space.begin();
       it != sample_space.end(); it++) {
    logprob_t weight = it->second / total_weight;
    alignment_t& alignment = it->first;

    size_t m = trg.size();
    size_t n = src.size();

    for (size_t i = 0; i < m; i++) {
      size_t j = alignment[i];
      lex.Increment(src[j], trg[i], weight);
      atable.Increment(j, i, m, n, weight);
      if (j != n-1)
	dtable.Increment(i, j, m, n, weight);
    }

    vector<unsigned> n_fert = fertility_count(alignment, instance);
    unsigned n_null = n_fert[n-1];
    genNull.Increment(1, n_null * weight);
    genNull.Increment(0, (m - 2*n_null) * weight);
    for (size_t j = 0; j < n-1; j++)
      ntable.Increment(src[j], n_fert[j], weight);
  }
}

vector<unsigned> Model3::fertility_count(alignment_t& al, TrainingInstance& instance) {
  sentence& src = instance.get_source();
  sentence& trg = instance.get_target();

  vector<unsigned> n_fert(src.size());
  for (size_t i = 0; i < trg.size(); i++) {
    n_fert[al[i]]++;
  }
  return n_fert;
}
  
logprob_t Model3::probability(alignment_t& al, TrainingInstance& instance) {
  sentence& src = instance.get_source();
  sentence& trg = instance.get_target();
  
  logprob_t prob = 1.0;
  size_t m = trg.size();
  size_t n = src.size();

  for (size_t i = 0; i < m; i++) {
    size_t j = al[i];
    prob *= lex.prob(src[j], trg[i]);
    if (j != n-1)
      prob *= dtable.prob(i, j, m, n);
  }

  vector<unsigned> n_fert = fertility_count(al, instance);
    
  unsigned n_null = n_fert[n-1];
  prob *= nChoosek(m - n_null, n_null) *
    pow(genNull.prob(1), n_null) *
    pow(genNull.prob(0), m - 2*n_null);
    
  for (size_t j = 0; j < n-1; j++)
    prob *= fact(n_fert[j]) * ntable.prob(src[j], n_fert[j]);
  return prob;
}

alignment_t Model3::align_words(TrainingInstance& instance) {
  alignment_t base = Model12::align_words(instance);
  logprob_t prob = probability(base, instance);
  pair<alignment_t, logprob_t> start = make_pair(base, prob);
  pair<alignment_t, logprob_t> peak = hillclimb(start, 0, instance, false);
  return peak.first;
}

bool Model3::good_fertility(vector<unsigned> fertility, TrainingInstance& instance) {
  sentence& src = instance.get_source();
  sentence& trg = instance.get_target();
  
  unsigned n_null = fertility[src.size()-1];
  if (2*n_null > trg.size())
    return false;
  // for (wordind_t j = 0; j < src.size()-1; j++)
  //   if (fertility[j] > 3)
  // 	return false;

  return true;
}

vector<pair<alignment_t, logprob_t>> Model3::random_sample(TrainingInstance& instance, int nSamples) {
  sentence& src = instance.get_source();
  sentence& trg = instance.get_target();
  
  vector<pair<alignment_t, logprob_t>> result;
  for (int n = 0; n < nSamples; n++) {
    alignment_t al;
    for (wordind_t i = 0; i < trg.size(); i++)
      al.push_back(rand() % src.size());
    logprob_t prob = probability(al, instance);
    result.push_back(make_pair(al, prob));
  }
  return result;
}
  
vector<pair<alignment_t, logprob_t>> Model3::sample(TrainingInstance& instance, bool use_peg) {
  sentence& src = instance.get_source();
  sentence& trg = instance.get_target();
  
  alignment_t base = Model12::align_words(instance);
  logprob_t prob = probability(base, instance);
  vector<unsigned> fertility = fertility_count(base, instance);
  if (!good_fertility(fertility, instance)) {
    cerr << "ERROR: the best from model2 is not a good enough alignment for model3\n";
    // print_alignment(base, cerr);
    return vector<pair<alignment_t, logprob_t>>();
  }

  pair<alignment_t, logprob_t> base_pair = make_pair(base, prob);
  if (!use_peg) {
    pair <alignment_t, logprob_t> peak = hillclimb(base_pair, 0, instance, false);
    return neighboring(peak, 0, instance, false);
  }
    
  vector<pair<alignment_t, logprob_t>> result;
  vector<unsigned> fert2;
  for (size_t i = 0; i < trg.size(); i++)
    for (size_t j = 0; j < src.size(); j++) {
      alignment_t pegged = base;
      pegged[i] = j;
      fert2 = fertility_count(pegged, instance);
      if (!good_fertility(fert2, instance))
	continue;

      prob_t score = score_of_move(instance, base, fertility,
				   i, j);
      pair<alignment_t, logprob_t> start = make_pair(pegged, prob*score);
      pair<alignment_t, logprob_t> climbed = hillclimb(start, i, instance);
      vector<pair<alignment_t, logprob_t>> neighbors = neighboring(climbed, i, instance);
      result.insert(result.end(), neighbors.begin(), neighbors.end());
    }
  return result;
}

pair<alignment_t, logprob_t> Model3::hillclimb(pair<alignment_t, logprob_t>& start, size_t t_pegged,
					       TrainingInstance& instance, bool use_peg) {
  pair<alignment_t, logprob_t> current = start;
  bool changed;
  do {
    changed = false;
    vector<pair<alignment_t, logprob_t>> neighbors = neighboring(current, t_pegged, instance, use_peg);
    logprob_t best_prob = current.second;
    for (vector<pair<alignment_t, logprob_t>>::iterator it = neighbors.begin();
	 it != neighbors.end(); it++) {
      logprob_t neighbor_prob = it->second;
      if (neighbor_prob > best_prob) {
	best_prob = neighbor_prob;
	current = *it;
	changed = true;
      }
    }
  } while (changed);
  return current;
}

  //based on GIZA implementation
prob_t Model3::score_of_swap(TrainingInstance& instance, alignment_t& alignment, wordind_t first_i, wordind_t second_i) {
  sentence& src = instance.get_source();
  sentence& trg = instance.get_target();
  
  if (first_i == second_i || alignment[first_i] == alignment[second_i])
    return 1.0;
    
  size_t m = trg.size();
  size_t n = src.size();
  wordind_t first_j = alignment[first_i];
  wordind_t second_j = alignment[second_i];
  prob_t score = lex.prob(src[first_j], trg[second_i]) * lex.prob(src[second_j], trg[first_i]) /
    (lex.prob(src[first_j], trg[first_i]) * lex.prob(src[second_j], trg[second_i]));
  if (first_j != n-1)
    score *= dtable.prob(second_i, first_j, m, n) / dtable.prob(first_i, first_j, m, n);
  if (second_j != n-1)
    score *= dtable.prob(first_i, second_j, m, n) / dtable.prob(second_i, second_j, m, n);

  return score;
}

  // These code strongly based on GIZA
prob_t Model3::score_of_move(TrainingInstance& instance, alignment_t& alignment, vector<unsigned>& fertility, wordind_t i, wordind_t new_j) {
  sentence& src = instance.get_source();
  sentence& trg = instance.get_target();

  size_t m = trg.size();
  size_t n = src.size();
  const wordind_t NULLIND = n-1;
  unsigned n_null = fertility[NULLIND];
  unsigned old_j = alignment[i];
    
  if (old_j == new_j)
    return 1.0;
    
  if (old_j == NULLIND)
    return (double)genNull.prob(0)*(double)genNull.prob(0)/(double)genNull.prob(1) *
      (n_null * (m-n_null+1)) / ((m-2*n_null+1) * (m-2*n_null+2)) *
      (fertility[new_j]+1) *
      (double)ntable.prob(src[new_j], fertility[new_j]+1) /
      (double)ntable.prob(src[new_j], fertility[new_j]) *
      (double)lex.prob(src[new_j], trg[i]) /
      (double)lex.prob(src[old_j], trg[i]) *
      (double)dtable.prob(i, new_j, m, n);

  if (new_j == NULLIND)
    return (double)genNull.prob(1)/(double)(genNull.prob(0)*genNull.prob(0)) *
      (double)(m-2*n_null) * (m-2*n_null-1) / ((n_null+1)*(m-n_null)) *
      (1.0 / fertility[old_j]) *
      (double)ntable.prob(src[old_j], fertility[old_j]-1) /
      (double)ntable.prob(src[old_j], fertility[old_j]) *
      (double)lex.prob(src[new_j], trg[i]) /
      (double)lex.prob(src[old_j], trg[i]) *
      (1.0 / (double)dtable.prob(i, old_j, m, n));

  // if (old_j != NULLIND && new_j != NULLIND)
  return (double)(fertility[new_j]+1) / fertility[old_j] *
    (double)ntable.prob(src[old_j], fertility[old_j]-1) /
    (double)ntable.prob(src[old_j], fertility[old_j]) *
    (double)ntable.prob(src[new_j], fertility[new_j]+1) /
    (double)ntable.prob(src[new_j], fertility[new_j]) *
    (double)lex.prob(src[new_j], trg[i]) /
    (double)lex.prob(src[old_j], trg[i]) *
    (double)dtable.prob(i, new_j, m, n) /
    (double)dtable.prob(i, old_j, m, n);
}
  
vector<pair<alignment_t, logprob_t>> Model3::neighboring(pair<alignment_t, logprob_t>& center, size_t t_pegged, TrainingInstance& instance, bool use_peg) {
  sentence& src = instance.get_source();
  sentence& trg = instance.get_target();
  
  vector<pair<alignment_t, logprob_t>> neighbors;

  alignment_t alignment = center.first;
  logprob_t prob = center.second;
    
  vector<unsigned> fertility = fertility_count(alignment, instance);
  vector<unsigned> fert2;
  for (size_t i = 0; i < trg.size(); i++) {
    if (i == t_pegged && use_peg)
      continue;
    for (size_t j = 0; j < src.size(); j++) {
      alignment_t new_al = alignment;
      new_al[i] = j;
      fert2 = fertility_count(new_al, instance);
      if (!good_fertility(fert2, instance))
	continue;

      prob_t score = score_of_move(instance, alignment, fertility,				     
				   i, j);
      // logprob_t p = probability(new_al, src, trg);

      // prob_t diff = (p/prob) / score;
      // if (diff > 1.01 || diff < 0.99) {
      //   if (alignment[i] == src.size()-1)
      //     cerr << "move from null\n";
      //   else if (j == src.size()-1)
      //     cerr << "move to null\n";
      //   else
      //     cerr << "usual move\n";
	
      //   cerr << "score of move " << score << "\n";
      //   cerr << "actual score" << p / prob << "\n";
      // }	  
	
      neighbors.push_back(make_pair(new_al, prob*score));
    }
  }

  for (size_t i = 0; i < trg.size(); i++) {
    if (i == t_pegged && use_peg)
      continue;
    for (size_t another_i = i+1; another_i < trg.size(); another_i++) {
      if (another_i == t_pegged && use_peg)
	continue;
      alignment_t new_al = alignment;
      swap(new_al[i], new_al[another_i]);

      fert2 = fertility_count(new_al, instance);
      if (!good_fertility(fert2, instance))
	continue;

      prob_t score = score_of_swap(instance, alignment, i, another_i);
      // logprob_t p = probability(new_al, src, trg);

      // prob_t diff = (p/prob) / score;
      // if (diff > 1.01 || diff < 0.99) {
      //   cerr << "score of swap " << score << "\n";
      //   cerr << "actual score" << p / prob << "\n";
      // }	  
      neighbors.push_back(make_pair(new_al, prob*score));	
    }
  }
  return neighbors;
}

void Model3::export_to_file(const char *lex_filename, const char *pos_filename, const char* a_filename, const char* n_filename) {
  // lex.ExportToFile(lex_filename, sent_handler.src_dict, sent_handler.trg_dict);
  // // if (use_pos)
  // //   pos.ExportToFile(pos_filename, sent_handler.src_pos_dict, sent_handler.trg_pos_dict);
  // // if (use_atable)
  // dtable.ExportToFile(a_filename);
    
  // ntable.ExportToFile(n_filename, sent_handler.src_dict);
}

