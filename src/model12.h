// Copyright 2015 by Bui Van Vuong
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
#pragma once
#include <unordered_map>
#include <iostream>
#include <cstdlib>
#include <cmath>
#include <utility>
#include <fstream>
#include <algorithm>
#include <numeric>
#include <getopt.h>

#include "src/dict.h"
#include "src/ttables.h"
#include "src/atables.h"
#include "src/types.h"
#include "src/bilingual_corpus.h"
#include "src/statistics.h"

using namespace std;

class Model12: public Model
{
public:
  Model12(Corpus &corpus, bool use_atable=false);
  alignment_t align_words(TrainingInstance& instance);

public:
  ATable atable;
  TTable lex;
  bool use_atable;
  bool use_ttable_smoothing;
  double alpha;
  Corpus* annotated_corpus;
  Statistics statistics;

  void set_annotated_corpus(Corpus* annotated_corpus);

  logprob_t sentence_prob(TrainingInstance& instance);
  double likelihood_of_alignment();
  unsigned error_counts();
  double smoothed_error_counts(double alpha = 20);

  vector<prob_t> alignment_probs(size_t i, TrainingInstance& instance,
					bool normalize=true);

  void do_statistics(TrainingInstance& instance);

  void em_iteration();
  double perplexity();
  /* double smooth_ttable(); */
  void set_smooth_for_ttable(double alpha);

};

double perplexity_wrapper(double alpha, void *params);
double likelihood_wrapper(double alpha, void *params);
double error_count_wrapper(double alpha, void *params);
double smoothed_error_count_wrapper(double alpha, void *params);

double smooth_ttable(Model12* model, double (*function)(double, void*));
