// Copyright 2015 by Bui Van Vuong
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
#pragma once
#include <unordered_map>
#include <iostream>
#include <cstdlib>
#include <cmath>
#include <utility>
#include <fstream>
#include <algorithm>
#include <numeric>
#include <getopt.h>

#include "src/dict.h"
#include "src/ttables.h"
#include "src/atables.h"
#include "src/types.h"
#include "src/bilingual_corpus.h"

using namespace std;

class Model
{
public:
  Model(Corpus &corpus);
  void train(int n_iterations);

  void align_words_in_corpus(const char* output_filename);

  virtual alignment_t align_words(TrainingInstance& instance) = 0;

public:
  Corpus &corpus;
  bool use_null;
  string name;

  void set_name(string name);
  virtual void do_statistics(TrainingInstance& instance) = 0;

  virtual void em_iteration();

  virtual double perplexity() = 0;

  vector<TrainingInstance> get_instances_for_training();
  vector<TrainingInstance> get_instances_for_validating();
};
