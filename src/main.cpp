#include <iostream>
#include <unordered_map>
#include <iostream>
#include <cstdlib>
#include <cmath>
#include <utility>
#include <fstream>
#include <algorithm>
#include <numeric>
#include <string.h>
#include <getopt.h>

#include "src/dict.h"
#include "src/ttables.h"
#include "src/atables.h"
#include "src/types.h"
#include "src/bilingual_corpus.h"
#include "src/model.h"
#include "src/model12.h"
// #include "src/modelc.h"
#include "src/model3.h"
#include <boost/math/tools/minima.hpp>
#include <functional>
using namespace std::placeholders;
using namespace std;

int main(int argc, char *argv[])
{
  int opt;
  char* src_filename = NULL;
  char* trg_filename = NULL;

  char* annotated_src_filename = NULL;
  char* annotated_trg_filename = NULL;
  char* annotated_al_filename = NULL;
  
  char* output_filename = NULL;

  double alpha = -1;
  
  int n_iterations[4];
  n_iterations[1] = 5;
  n_iterations[2] = 3;
  n_iterations[3] = 0;
  bool use_peg = false;
  // bool use_context = false;
  vector<string> src_context_fnames;
  const char* error_count_type = "none"; // use likelihood of alignment instead
  while ((opt = getopt(argc, argv, "s:t:S:T:A:pa:c:o:1:2:3:e:")) != -1) {
    switch (opt) {
    case 's':
      src_filename = optarg;	
      break;
    case 't':
      trg_filename = optarg;     
      break;
    case 'a':
      alpha = atof(optarg);
      break;
    case 'e':
      error_count_type = optarg;
      break;
    case 'p':
      use_peg = true;
      break;
    case 'S':
      annotated_src_filename = optarg;
      break;
    case 'T':
      annotated_trg_filename = optarg;
      break;
    case 'A':
      annotated_al_filename = optarg;
      break;
    case 'o':
      output_filename = optarg;
      break;
    case '1':
      n_iterations[1] = atoi(optarg);
      break;
    case '2':
      n_iterations[2] = atoi(optarg);
      break;
    case '3':
      n_iterations[3] = atoi(optarg);
      break;
    // case 'c':
    //   use_context = true;
    //   src_context_fnames.push_back(optarg);
    //   break;
    }
  }

  for (string s: src_context_fnames)
    cout << "context: " << s << endl;
  
  Corpus corpus(src_filename, trg_filename, src_context_fnames);
  corpus.src_d.set_frozen(true);
  corpus.trg_d.set_frozen(true);
  
  cout << "#target words = " << corpus.trg_d.max() << endl;
  bool use_annotated_corpus = annotated_src_filename && annotated_trg_filename && annotated_al_filename;
  
  Model12 *m;
  // if (use_context) {
  //   m = new ModelC(corpus);
  //   m->train(7);
  //   m->align_words_in_corpus(output_filename);
  //   return 0;
  // }

  m = new Model12(corpus);
  // Model12& model12 = *((Model12*) m);

  Corpus *annotated_corpus;
  if (use_annotated_corpus) {
    annotated_corpus = new Corpus(annotated_src_filename, annotated_trg_filename, vector<string>(), true,
				  &corpus.src_d, &corpus.trg_d);
    annotated_corpus->set_alignment_file(annotated_al_filename);
    m->set_annotated_corpus(annotated_corpus);
  }

  double (*objective_function)(double, void*) = perplexity_wrapper;
  if (use_annotated_corpus) {
    if (strcmp(error_count_type, "smooth") == 0)
      objective_function = smoothed_error_count_wrapper;
    else if (strcmp(error_count_type, "unsmooth") == 0)
      objective_function = error_count_wrapper;
    else
      objective_function = likelihood_wrapper;
  }

  // m->set_name("model1");  

  if (n_iterations[1] > 0) {
    if (alpha == -1) {
      // if (use_annotated_corpus) {
      // 	alpha = smooth_ttable(m, likelihood_wrapper);      
      // } else {    
      // 	alpha = smooth_ttable(m, perplexity_wrapper);
      // }
      // alpha = smooth_ttable(m, objective_function);
      typedef std::pair<double, double> Result;
      Result r2 = boost::math::tools::brent_find_minima(std::bind(objective_function, _1, (void*) m), 0.0, 10.0, 16);
      alpha = r2.first;
    }
    // double add_n = 0.0014120;
    // double add_n = 0.01;
     // add_n = 0.0;
    
    cout << "running on alpha = " << alpha << endl;
    m->set_smooth_for_ttable(alpha);
    m->train(n_iterations[1]);
  }

  if (n_iterations[2] > 0) {
    // m->set_name("model2");
    m->use_atable = true;
    m->train(n_iterations[2]);
  }

  Model3 model3(*m, use_peg);

  if (n_iterations[3] > 0) {
    m = &model3;
    // m->set_name("model3");
    m->train(n_iterations[3]);
  }

  m->align_words_in_corpus(output_filename);
  return 0;
}
