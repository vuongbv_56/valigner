#ifndef _TYPES_H_
#define _TYPES_H_

#include <vector>
#include "src/logprob.h"

typedef std::vector<unsigned> sentence;
typedef size_t wordind_t;
typedef std::vector<wordind_t> alignment_t;
typedef double prob_t;
typedef LogProb logprob_t;
typedef std::vector<unsigned> context_t;

#endif
