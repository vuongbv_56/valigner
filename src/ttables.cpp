// Copyright 2013 by Bui Van Vuong
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include <cmath>
#include <fstream>
#include <vector>
#include <numeric>
#include <fstream>

#include "src/types.h"
#include "src/ttables.h"
#include "src/dict.h"

MapTable::MapTable() {
  this->real_n_entries = 10.000;
}

void MapTable::set_count_map_init(std::unordered_map<unsigned, double> count_map_init) {
  this->count_map_init = count_map_init;
}

inline prob_t MapTable::prob(unsigned k) const {
  const std::unordered_map<unsigned, prob_t>::const_iterator it =
    prob_map.find(k);
    
  if (it == prob_map.end())
    return THRESHOLD;
  else if (it->second < THRESHOLD)
    return THRESHOLD;
  else
    return it->second;
}

inline void MapTable::Increment(unsigned k, prob_t x) {
  count_map[k] += x;
}
  
void MapTable::set_real_number_of_entries(unsigned n_entries) {
  real_n_entries = n_entries;
}

void MapTable::Normalize(double alpha) {
  prob_t total = 0.0;

  // for (auto& it: count_map) {
  //   total += it.second;
  //   it.second += alpha;
  // }

  // total += alpha * real_n_entries;

  for (auto& it: count_map) {
    std::unordered_map<unsigned, double>::iterator added = count_map_init.find(it.first);
    if (added != count_map_init.end())
      it.second += alpha * added->second;
    total += it.second;
  }

  // for (auto& it: count_map) {
  //   total += it.second;
  // }
  // for (auto& it: count_map) {
  //   it.second += total * alpha;
  // }

  // total += (total * alpha) * real_n_entries;

  prob_map.clear();
  for (std::unordered_map<unsigned, prob_t>::iterator it = count_map.begin();
       it != count_map.end();
       it++) {
    prob_t prob = it->second / total;
    if (prob > THRESHOLD)
      prob_map.emplace(it->first, prob);
  }
}

TTable::TTable(Corpus& corpus) :
  corpus(corpus),
  ttable(corpus.src_d.size()),
  statistics(corpus) {
  // for (std::vector<MapTable>::iterator it = ttable.begin();
  //      it != ttable.end();
  //      it++) {
  for (unsigned s = 0; s < statistics.get_n_source_words(); s++) {
    // it->set_real_number_of_entries(corpus.trg_d.size());
    ttable[s].set_real_number_of_entries(corpus.trg_d.size());
    // ttable[s].set_count_map_init(statistics.src_trg_counts[s]);
    std::unordered_map<unsigned, double> count_map_init;
    std::unordered_map<unsigned, unsigned>& counts = statistics.src_trg_counts[s];
    for (std::unordered_map<unsigned, unsigned>::iterator it = counts.begin();
	 it != counts.end();
	 it++) {
      unsigned t = it->first;
      count_map_init.emplace(t, 2 * ((double) counts[t]) /
			     (statistics.get_n_occurences_of_source_word(s) +
			      statistics.get_n_occurences_of_target_word(t)));
    }
    ttable[s].set_count_map_init(count_map_init);
  }
}

prob_t TTable::prob(unsigned s, unsigned t) const {
  if (s < ttable.size())
    return ttable[s].prob(t);
  else
    return THRESHOLD;
}

void TTable::Increment(unsigned s, unsigned t, prob_t x) {
  if (s >= ttable.size())
    ttable.resize(s+1);
  ttable[s].Increment(t, x);
}

void TTable::Normalize(double alpha) {
  for (size_t s = 0; s < ttable.size(); s++)
    // ttable[s].Normalize(forget_count, alpha);
    // ttable[s].Normalize(alpha * statistics.get_n_occurences_of_source_word(s));
    ttable[s].Normalize(alpha);
}

void TTable::Increment(prob_t x) {
  for (unsigned i = 0; i < ttable.size(); ++i) {
    std::unordered_map<unsigned, prob_t>& counts = ttable[i].count_map;
    for (std::unordered_map<unsigned, prob_t>::iterator it = counts.begin();
	 it != counts.end(); ++it) {
      it->second += x;
    }
  }
}

void TTable::ExportToFile(const char* filename, Dict& sd, Dict& td) {
  std::ofstream file(filename);
  for (unsigned i = 0; i < ttable.size(); ++i) {
    const std::string& a = sd.Convert(i);
    std::unordered_map<unsigned, prob_t>& cpd = ttable[i].prob_map;
    for (std::unordered_map<unsigned, prob_t>::iterator it = cpd.begin();
	 it != cpd.end(); ++it) {
      const std::string& b = td.Convert(it->first);
      prob_t c = it->second;
      file << a << '\t' << b << '\t' << c << std::endl;
    }
  }
  file.close();
}
