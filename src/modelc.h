// Copyright 2015 by Bui Van Vuong
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
#pragma once
#include <unordered_map>
#include <iostream>
#include <cstdlib>
#include <cmath>
#include <utility>
#include <fstream>
#include <algorithm>
#include <numeric>
#include <getopt.h>

#include "src/dict.h"
#include "src/ttables.h"
#include "src/atables.h"
#include "src/types.h"
#include "src/bilingual_corpus.h"
#include "src/atables.h"

using namespace std;

typedef enum {TUNING_TRANSLATION, TUNING_WEIGHT} phase_t;

class ModelC: public Model
{
public:
  ModelC(Corpus &corpus, bool use_atable=false);
  alignment_t align_words(TrainingInstance& instance);

public:
  ATable atable;
  /* TTable lex; */
  vector<TTable> ctables;
  VectorTable weights;
  phase_t phase;
  bool use_atable;
  logprob_t sentence_prob(TrainingInstance& instance);
    
  vector<prob_t> alignment_probs(size_t i, TrainingInstance& instance, bool normalize=true);
  prob_t prob_of_link(wordind_t i, wordind_t j, TrainingInstance& instance);

  void do_statistics(TrainingInstance& instance);

  void em_iteration();
  double perplexity();
};
