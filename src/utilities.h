// Copyright 2015 by Bui Van Vuong
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#pragma once

unsigned nChoosek(unsigned n, unsigned k);
unsigned fact(unsigned n);
/* void print_alignment(alignment_t& alignment, ostream& os) { */
/*   for (wordind_t i = 0; i < alignment.size(); i++) */
/*     os << i+1 << "-" << alignment[i]+1 << " "; */
/*   os << "\n"; */
/* } */
