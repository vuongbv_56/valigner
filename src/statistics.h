// Copyright 2015 by Bui Van Vuong
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#pragma once

#include <iostream>
#include <fstream>
#include <src/types.h>
#include <vector>
#include <algorithm>
#include "src/bilingual_corpus.h"


class Statistics {
 public:
  Corpus& corpus;
  std::vector<unsigned> src_counts;
  std::vector<unsigned> trg_counts;
  std::vector<std::unordered_map<unsigned, unsigned>> src_trg_counts;
  
  Statistics(Corpus& corpus);
  unsigned get_n_source_words();
  unsigned get_n_target_words();

  unsigned get_n_occurences_of_source_word(unsigned s);
  unsigned get_n_occurences_of_target_word(unsigned t);

  unsigned get_n_co_occurences_of_pair(unsigned s, unsigned t);
};
