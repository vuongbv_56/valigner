// Copyright 2015 by Bui Van Vuong
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
#include <cmath>
#include <fstream>
#include <utility>
#include <vector>
#include <numeric>
#include <fstream>
#include "src/types.h"
#include "src/atables.h"

#define THRESHOLD prob_t(1e-9)
VectorTable::VectorTable(size_t capacity) {
  prob_vector.resize(capacity);
  count_vector.resize(capacity);
  for (size_t i = 0; i < capacity; i++)
    prob_vector[i] = 1.0 / capacity;
}

prob_t VectorTable::prob(unsigned k) const {
  return prob_vector[k];
}

void VectorTable::Increment(unsigned k, prob_t x) {
  count_vector[k] += x;
}

void VectorTable::Normalize() {
  /* std::cerr << "VectorTable normalizing\n"; */
  prob_t total = accumulate(count_vector.begin(), count_vector.end(), 0.0);
  if (!total)
    total = 1;
    
  for (size_t i = 0; i < prob_vector.size(); i++) {
    prob_vector[i] = count_vector[i] / total;
    count_vector[i] = 0.0;
  }
}

_ATable::_ATable() {
  std::cerr << "Empty constructor <<WARNING>>\n";
}
  
_ATable::_ATable(unsigned m, unsigned n)
  :ttable(m, VectorTable(n)){
  /* :default_table(n) { */
  /* VectorTable table(n); */
  /* ttable.resize(m, table); */
}
    
prob_t _ATable::prob(unsigned s, unsigned t) const {
  /* if (s < ttable.size()) */
  return ttable[s].prob(t);
  /* else */
  /*   return THRESHOLD; */
}

void _ATable::Increment(unsigned s, unsigned t, prob_t x) {
  /* if (s >= ttable.size()) */
  /*   ttable.resize(s+1, default_table); */
  ttable[s].Increment(t, x);
}


void _ATable::Normalize() {
  /* std::cerr << "atable normalizing\n"; */
  for (size_t s = 0; s < ttable.size(); s++)
    ttable[s].Normalize();
}

size_t PairHash::operator()(const std::pair<wordind_t,wordind_t>& x) const {
  return (unsigned wordind_t)x.first << 16 | (unsigned)x.second;
}


ATable::ATable() {
}

prob_t ATable::prob(wordind_t j, wordind_t i, wordind_t m, wordind_t n) {
  std::pair<wordind_t, wordind_t> length_pair = std::make_pair(m, n);
  std::unordered_map<std::pair<wordind_t, wordind_t>, _ATable, PairHash>::iterator it =
    atable.find(length_pair);
  return it != atable.end()? atable[length_pair].prob(i, j): THRESHOLD;
}
  
void ATable::Increment(wordind_t j, wordind_t i, wordind_t m, wordind_t n, prob_t x) {
  std::pair<wordind_t, wordind_t> length_pair = std::make_pair(m, n);
  if (atable.find(length_pair) == atable.end()) {
    /* VectorTable table(n); */
    /* TTable ttable(m, table); */
    atable.emplace(length_pair, _ATable(m, n));
  }
  atable[length_pair].Increment(i, j, x);
}

void ATable::Normalize() {
  for (std::unordered_map<std::pair<wordind_t, wordind_t>, _ATable, PairHash>::iterator it = atable.begin(); it != atable.end(); ++it) {
    it->second.Normalize();
  }
}

void ATable::ExportToFile(const char* filename) {
  std::ofstream file(filename);

  for (std::unordered_map<std::pair<wordind_t, wordind_t>, _ATable, PairHash>::iterator it = atable.begin(); it != atable.end(); ++it) {
    std::pair<wordind_t, wordind_t> length_pair = it->first;
    wordind_t m = length_pair.first;
    wordind_t n = length_pair.second;
      
    _ATable& table = it->second;
    std::vector<VectorTable> ttable = table.ttable;
    for (wordind_t i = 0; i < m; i++) {
      VectorTable vt = ttable[i];
      for (wordind_t j = 0; j < n; j++) {
	prob_t c = vt.prob(j);
	file << j << " " << i << " " << m << " " << n << "\t" << c << std::endl;
      }
    }
  }
  file.close();
}
