// Copyright 2013 by Bui Van Vuong
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
#ifndef _TTABLES_H_
#define _TTABLES_H_

#include <cmath>
#include <fstream>
#include <vector>
#include <numeric>
#include <fstream>

#include "src/types.h"
#include "src/dict.h"
#include "src/statistics.h"
#define THRESHOLD prob_t(1e-9)
class MapTable {
 public:
  std::unordered_map<unsigned, prob_t> prob_map;
  std::unordered_map<unsigned, prob_t> count_map;
  std::unordered_map<unsigned, double> count_map_init;
  unsigned real_n_entries;
 public:
  MapTable();
  void set_count_map_init(std::unordered_map<unsigned, double> count_map_init);
  inline prob_t prob(unsigned k) const;
  inline void Increment(unsigned k, prob_t x=1.0);
  void Normalize(double add_n=0.0);
  void set_real_number_of_entries(unsigned n_entries);
};

class TTable {
 public:
  Corpus& corpus;
  std::vector<MapTable> ttable;
  Statistics statistics;
  
  TTable(Corpus& corpus);
  prob_t prob(unsigned s, unsigned t) const;
  void Increment(unsigned s, unsigned t, prob_t x=1.0);
  void Increment(prob_t x=1.0);
  void Normalize(double add_n=0.0);
  void ExportToFile(const char* filename, Dict& sd, Dict& td);
};
#endif
