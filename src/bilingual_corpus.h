// Copyright 2015 by Bui Van Vuong
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#pragma once

#include <iostream>
#include <fstream>
#include <src/types.h>
#include <vector>
#include <algorithm>

#include "src/dict.h"
#include "src/types.h"

class Corpus;

class TrainingInstance {
  Corpus& corpus;
  sentence src, trg;
  alignment_t alignment;
  std::vector<context_t> src_context;
  
 public:  
  TrainingInstance(Corpus& corpus, std::string& src, std::string& trg);  		   
  
  sentence& get_source();
  sentence& get_target();
  alignment_t& get_alignment();
  std::vector<context_t>& get_source_context();

  void set_alignment(alignment_t &alignment);
  void set_source_context(std::vector<std::string>& src_context);
};

class Corpus {
 public:
  Dict src_d;
  Dict trg_d;
  std::vector<Dict> src_context_d;
  unsigned src_null;
  unsigned number_of_context;

  bool use_null;
  bool has_alignment;

  std::vector<TrainingInstance> instances;

  Corpus(const char* src_filename, const char* trg_filename, std::vector<std::string> src_context_filenames,      
	 bool use_null=true, Dict* src_dp = NULL, Dict* trg_dp = NULL);
  void set_alignment_file(const char* alignment_file);
  
  std::vector<TrainingInstance>& get_training_instances();

  void get_canonical_form(sentence& src, sentence& trg,
			  std::string& canon_src, std::string& canon_trg);
  int get_number_of_context();
};
