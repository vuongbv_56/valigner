#pragma once
#include "ttables.h"
#include "src/types.h"

class NTable: public TTable {
 public:
  NTable(Corpus& corpus);
  void ExportToFile(const char* filename, Dict& sd);
};
