// Copyright 2015 by Bui Van Vuong
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
#pragma once

#include <cmath>
#include <fstream>
#include <utility>
#include <vector>
#include <numeric>
#include <fstream>
#include <unordered_map>

#include "src/types.h"

#define THRESHOLD prob_t(1e-9)

class VectorTable {
 private:
  std::vector<prob_t> prob_vector;
  std::vector<prob_t> count_vector;
 public:
  VectorTable(size_t capacity);
  prob_t prob(unsigned k) const;
  void Increment(unsigned k, prob_t x=1.0);
  void Normalize(); 
};


class _ATable {
 public:
  std::vector<VectorTable> ttable;
  _ATable();
  _ATable(unsigned m, unsigned n);
    
  prob_t prob(unsigned s, unsigned t) const;

  void Increment(unsigned s, unsigned t, prob_t x=1.0);
  void Normalize();
};

struct PairHash {
  size_t operator()(const std::pair<wordind_t,wordind_t>& x) const;
};


class ATable {
 public:
  std::vector<std::pair<wordind_t, wordind_t> > size_pairs;

  ATable();
  prob_t prob(wordind_t j, wordind_t i, wordind_t m, wordind_t n);
  
  void Increment(wordind_t j, wordind_t i, wordind_t m, wordind_t n, prob_t x=1.0);
  void Normalize();

  void ExportToFile(const char* filename); 
 public:
  std::unordered_map<std::pair<wordind_t, wordind_t>, _ATable, PairHash> atable;
};
