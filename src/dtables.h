#pragma once

#include "src/atables.h"

class DTable: public ATable {
 public:
  prob_t prob(wordind_t i, wordind_t j, wordind_t m, wordind_t n);

  void Increment(wordind_t i, wordind_t j, wordind_t m, wordind_t n, prob_t x=1.0);
  void ExportToFile(const char* filename);
};

