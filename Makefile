CC = g++
CFLAGS = -g -O3 -Wall -std=c++11 -I. 
LD = ld

# valigner: dict.o bilingual_corpus.o ttables.o atables.o dtables.o ntables.o model.o model12.o modelc.o model3.o main.o logprob.o utilities.o
# 	$(CC) $(CFLAGS) dict.o bilingual_corpus.o ttables.o atables.o dtables.o ntables.o model.o model12.o modelc.o model3.o main.o utilities.o logprob.o -o valigner -lgsl -lgslcblas


valigner: dict.o bilingual_corpus.o ttables.o atables.o dtables.o ntables.o model.o model12.o model3.o main.o logprob.o utilities.o statistics.o
	$(CC) $(CFLAGS) dict.o bilingual_corpus.o ttables.o atables.o dtables.o ntables.o model.o model12.o model3.o main.o utilities.o logprob.o statistics.o -o valigner -lgsl -lgslcblas

dict.o: src/dict.cpp
	$(CC) $(CFLAGS) -c src/dict.cpp

ttables.o: src/ttables.cpp
	$(CC) $(CFLAGS) -c src/ttables.cpp

atables.o: src/atables.cpp
	$(CC) $(CFLAGS) -c src/atables.cpp

dtables.o: src/dtables.cpp
	$(CC) $(CFLAGS) -c src/dtables.cpp

ntables.o: src/ntables.cpp
	$(CC) $(CFLAGS) -c src/ntables.cpp

bilingual_corpus.o: src/bilingual_corpus.cpp dict.o
	$(CC) $(CFLAGS) -c src/bilingual_corpus.cpp

model.o: src/model.cpp
	$(CC) $(CFLAGS) -c src/model.cpp

model12.o: src/model12.cpp
	$(CC) $(CFLAGS) -c src/model12.cpp

# modelc.o: src/modelc.cpp
# 	$(CC) $(CFLAGS) -c src/modelc.cpp

model3.o: src/model3.cpp
	$(CC) $(CFLAGS) -c src/model3.cpp

main.o: src/main.cpp
	$(CC) $(CFLAGS) -c src/main.cpp

logprob.o: src/logprob.cpp
	$(CC) $(CFLAGS) -c src/logprob.cpp

utilities.o: src/utilities.cpp
	$(CC) $(CFLAGS) -c src/utilities.cpp

statistics.o: src/statistics.cpp
	$(CC) $(CFLAGS) -c src/statistics.cpp

clean:
	rm *.o
